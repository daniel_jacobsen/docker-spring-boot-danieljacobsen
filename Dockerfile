FROM openjdk:12
ADD target/docker-spring-boot-danieljacobsen.jar docker-spring-boot-danieljacobsen.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-danieljacobsen.jar"]
